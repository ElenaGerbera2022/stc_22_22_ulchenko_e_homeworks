package ru.inno.ec.security.config.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Group;
import ru.inno.ec.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {

    List<User> findAllByStateNot(User.State state);

    List<User> findAllByGroupsNotContainsAndState(Group group, User.State state);

    List<User> findAllByGroupsContains(Group group);

    Optional<User> findByLogin(String login);

}
