import java.util.List;

public interface ProductRepository {
    public List<Product> AllProducts();

    public Product findById(int Id);

    List<Product> findAllByTitleLike(String title);

    public void update(Product productIn);
}
