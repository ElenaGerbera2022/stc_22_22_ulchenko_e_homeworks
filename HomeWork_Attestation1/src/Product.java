public class Product {
    private final int id;
    private final String titleProduct;
    private Float priceProduct;
    private Integer amountProduct;

    public Product(Integer id, String titleProduct, Float priceProduct, Integer amountProduct) {
        this.id = id;
        this.titleProduct = titleProduct;
        this.priceProduct = priceProduct;
        this.amountProduct = amountProduct;
    }

    public Integer getId() {
        return id;
    }

    public String getTitleProduct() {
        return titleProduct;
    }

    public void setAmountProduct(Integer amountProduct) {
        this.amountProduct = amountProduct;
    }

    public Float getPriceProduct() {
        return priceProduct;
    }

    public Integer getAmountProduct() {
        return amountProduct;
    }

    public void setPriceProduct(Float priceProduct) {
        this.priceProduct = priceProduct;
    }

//    public void setAmountProduct() {
//        this.amountProduct = amountProduct;
//    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", titleProduct='" + titleProduct + '\'' +
                ", priceProduct=" + priceProduct + '\'' +
                ", amountProduct=" + amountProduct +
                '}';
    }

    public String toLine() {
        return id + "|" + titleProduct + "|" + priceProduct + "|" + amountProduct;
    }
}
