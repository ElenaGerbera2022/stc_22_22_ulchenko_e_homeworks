public class Main {
    public static void main(String[] args) {
        ProductRepository productRepository = new ProductRepositoryFileBasedImpl("список продуктов.txt");

        assert ("5|Арбуз|233.8|46".equals(productRepository.findById(5).toLine()));
        assert ("50|Антильский абрикос|76.0|40".equals(productRepository.findById(50).toLine()));
        assert ("340|Соль столовая|132.0|10".equals(productRepository.findById(340).toLine()));

        assert (productRepository.findAllByTitleLike("ябЛОки").toString().contains("Product{id=28, titleProduct='Яблоки запечённые', priceProduct=182.5', amountProduct=24"));
        assert (productRepository.findAllByTitleLike("ябЛОки").toString().equals("[Product{id=27, titleProduct='Яблоки', priceProduct=157.9', amountProduct=48}, Product{id=28, titleProduct='Яблоки запечённые', priceProduct=182.5', amountProduct=24}, Product{id=29, titleProduct='Яблоки Голден', priceProduct=142.7', amountProduct=17}, Product{id=30, titleProduct='Яблоки Гренни Смит', priceProduct=78.8', amountProduct=31}, Product{id=31, titleProduct='Яблоки красные', priceProduct=153.8', amountProduct=34}, Product{id=101, titleProduct='Яблоки сушёные', priceProduct=183.5', amountProduct=30}, Product{id=102, titleProduct='Яблоки вяленые', priceProduct=147.6', amountProduct=7}]"));

        int testFindProduct = 5;
        Product testProduct = productRepository.findById(testFindProduct);
        testProduct.setPriceProduct(200f);
        testProduct.setAmountProduct(20);
        productRepository.update(testProduct);
        assert (productRepository.findById(testFindProduct).toLine().equals("5|Арбуз|200.0|20"));

        testFindProduct = 20;
        testProduct = productRepository.findById(testFindProduct);
        testProduct.setPriceProduct(10f);
        testProduct.setAmountProduct(80);
        productRepository.update(testProduct);
        assert (productRepository.findById(testFindProduct).toLine().equals("20|Оливки зелёные|10.0|80"));

        testFindProduct = 340;
        testProduct = productRepository.findById(testFindProduct);
        testProduct.setPriceProduct(25f);
        testProduct.setAmountProduct(12);
        productRepository.update(testProduct);
        assert (productRepository.findById(testFindProduct).toLine().equals("340|Соль столовая|25.0|12"));
    }
}