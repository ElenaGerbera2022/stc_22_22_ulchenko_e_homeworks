package ru.inno.ec.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.inno.ec.models.User;

import javax.management.relation.Role;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserForm {
    // @Size(min = 3, max = 20)
    private String firstName;

    //@NotNull
    // @NotBlank
    private String lastName;
    private String login;
    private String password;
    private Integer age;
}