package ru.inno.util.repository;
import ru.inno.util.models.Car;
import java.util.List;

public interface CarsRepository {

    List<Car> findAll();

    void save(Car student);
}
