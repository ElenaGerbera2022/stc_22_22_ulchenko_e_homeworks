package ru.inno.util.repository;

import ru.inno.util.models.Car;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CarsRepositoryJdbsImpl implements CarsRepository {

    //Language=SQL
    private static final String SQL_SELECT_ALL = "select * from car order by id";

    //Language=SQL
    private static final String SQL_INSERT = "insert into car(model, color, number, driver_id) values(?, ?, ?, ?)";

    private final DataSource dataSource;

    public CarsRepositoryJdbsImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Car> userRowMapper = row -> {
        try {
            return Car.builder()
                    .id(row.getInt("id"))
                    .model(row.getString("model"))
                    .color(row.getString("color"))
                    .number(row.getString("number"))
                    .driver_id(row.getInt("driver_id"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car car = userRowMapper.apply(resultSet);
                    cars.add(car);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return cars;
    }

    @Override
    public void save(Car car) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, car.getModel());
            preparedStatement.setString(2, car.getColor());
            preparedStatement.setString(3, car.getNumber());
            preparedStatement.setInt(4, car.getDriver_id());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert car");
            }

            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    car.setId(generatedId.getInt("id"));
                } else {
                    throw new SQLException("Can't obtain generated id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
