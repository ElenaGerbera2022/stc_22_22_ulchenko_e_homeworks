import java.io.*;
import java.util.List;

public class ProductRepositoryFileBasedImpl implements ProductRepository {

    private final String fileName;

    public ProductRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Product> AllProducts() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return bufferedReader
                    .lines()
                    .map(this::parseProduct)
                    .toList();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public Product findById(int id) {
        List<Product> allProducts = AllProducts();
        return allProducts
                .stream()
                .filter(product -> product.getId().equals(id))
                .findFirst()
                .get();
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        List<Product> allProducts = AllProducts();
        return allProducts
                .stream()
                .filter(product -> product.getTitleProduct().toLowerCase().contains(title.toLowerCase()))
                .toList();
    }

    @Override
    public void update(Product productIn) {
        List<Product> allProducts = AllProducts();
        allProducts.stream()
                .filter(product -> product.getId().equals(productIn.getId()))
                .forEach(product -> {product.setAmountProduct(productIn.getAmountProduct()); product.setPriceProduct(productIn.getPriceProduct());});
        writeAllProduct(allProducts);
    }

    private void writeAllProduct(List<Product> products) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))) {
            for (Product product : products) {
                String stringInWrite = product.toLine();
                bufferedWriter.write(stringInWrite);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    private Product parseProduct(String line) {
        String[] parts = line.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String titleProduct = parts[1];
        Float priceProduct = Float.parseFloat(parts[2]);
        Integer amountProduct = Integer.parseInt(parts[3]);
        return new Product(id, titleProduct, priceProduct, amountProduct);
    }
}
