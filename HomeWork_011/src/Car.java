public class Car {
    private final String num;
    private final String model;
    private final String color;
    private final Integer mileage;
    private final Integer price;

    public Car(String num, String model, String color, Integer mileage, Integer price) {
        this.num = num;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

//    @Override
//    public String toString() {
//        return "Car{" +
//                "num='" + num + '\'' +
//                ", model='" + model + '\'' +
//                ", color='" + color + '\'' +
//                ", mileage=" + mileage +
//                ", price=" + price +
//                '}';
//    }

    public String getNum() {
        return num;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public Integer getPrice() {
        return price;
    }
//    private Car parseCarNew(String line) {
//        String[] parts = line.split("\\|");
//        String num = parts[0];
//        String model = parts[1];
//        String color = parts[2];
//        Integer mileage = Integer.parseInt(parts[3]);
//        Integer price = Integer.parseInt(parts[4]);
//        return new Car(num, model, color, mileage, price);
//    }
}
