package ru.inno.ec.services;

import ru.inno.ec.models.Group;
import ru.inno.ec.models.User;

import java.util.List;

public interface GroupsService {
    void addUserToGroup(Long groupId, Long userId);

    void addGroup(Group group);
    List<Group> getAllGroups();

    Group getGroup(Long groupId);


    List<User> getNotInGroupUsers(Long groupId);

    List<User> getInGroupUsers(Long groupId);

    void deleteGroup(Long userId);

    void updateGroup(Long groupId, Group group);
}
