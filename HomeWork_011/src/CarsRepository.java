import java.util.List;

public interface CarsRepository {
    List<Car> readAllCars();
    //    String toString();
    Car parseCar(String line);
    public Long filterDistinctCarPriceFromTo(int from, int to);
    public String filterCarToColorOrMileage(String color, Integer mileage);
    public Double getAveragePrice(String modelCar);
    public String filterColorOfMinPrice();
    public String filterColorOfMinPriceOther();

    public List<String> filterCarToColorOrMileageList(String color, Integer mileage);
}
