package ru.inno.util.models;

import lombok.*;
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder

public class Car {
    private Integer id;
    private String model;
    private String color;
    private String number;
    private Integer driver_id;
}
