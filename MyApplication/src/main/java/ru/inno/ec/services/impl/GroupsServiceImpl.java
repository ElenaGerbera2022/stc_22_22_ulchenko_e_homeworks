package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.models.Group;
import ru.inno.ec.models.User;
import ru.inno.ec.security.config.repositories.GroupRepository;
import ru.inno.ec.security.config.repositories.PostsRepository;
import ru.inno.ec.security.config.repositories.UsersRepository;
import ru.inno.ec.services.GroupsService;

import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
@Service
public class GroupsServiceImpl implements GroupsService {

    private final GroupRepository groupsRepository;
    private final UsersRepository usersRepository;
    private final PostsRepository postsRepository;

    @Override
    public void addUserToGroup(Long groupId, Long userId) {
        Group group = groupsRepository.findById(groupId).orElseThrow();
        User user = usersRepository.findById(userId).orElseThrow();
        user.getGroups().add(group);
        usersRepository.save(user);
    }

    @Override
    public void addGroup(Group group) {
        Group newGroup = Group.builder()
                .description(group.getDescription())
                .title(group.getTitle())
                .build();
        groupsRepository.save(newGroup);
    }

    @Override
    public List<Group> getAllGroups() {
        return groupsRepository.findAll();
    }

    @Override
    public Group getGroup(Long groupId) {
        return groupsRepository.findById(groupId).orElseThrow();
    }

    @Override
    public List<User> getNotInGroupUsers(Long groupId) {
        Group group = groupsRepository.findById(groupId).orElseThrow();
        return usersRepository.findAllByGroupsNotContainsAndState(group, User.State.CONFIRMED);
    }

    @Override
    public List<User> getInGroupUsers(Long groupId) {
        Group group = groupsRepository.findById(groupId).orElseThrow();
        return usersRepository.findAllByGroupsContains(group);
    }
//  рабочая версия
//    @Override
//    public void deleteGroup(Long groupId) {
//        groupsRepository.deleteById(groupId);
//    }


    @Override
    public void deleteGroup(Long groupId) {
        groupsRepository.deleteById(groupId);
    }

    @Override
    public void updateGroup(Long groupId, Group group) {

        Group groupForUpdate = groupsRepository.findById(groupId).orElseThrow();
        groupForUpdate.setTitle(group.getTitle());
        groupForUpdate.setDescription(group.getDescription());
        groupsRepository.save(groupForUpdate);
    }

//    @Override
//    public void updateChat(Long chatId, Chat chat) {
//        Chat chatForUpdate = chatsRepository.findById(chatId).orElseThrow();
//        chatForUpdate.setName(chat.getName());
//        chatsRepository.save(chatForUpdate);
//    }

//    Chat chat = chatsRepository.findById(chatId).orElseThrow();
//
//    Set<User> usersInChat = chat.getUsers();
//        usersInChat.forEach(user -> {
//        user.getChats().remove(chat);
//        usersRepository.save(user);
//    });
}

