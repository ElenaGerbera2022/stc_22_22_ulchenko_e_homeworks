insert into driver (first_name, last_name, phone_number, experience, age, has_license, category, rating)
values ('Smith', 'Andry', '+8968574123', 5, 28, true, 'A B', 5);
insert into driver (first_name, last_name, phone_number, experience, age, has_license, category, rating)
values ('Parker', 'Piter', '+8124578521', 7, 28, true, 'A B C', 4);
insert into driver (first_name, last_name, phone_number, experience, age, has_license, category, rating)
values ('Anderson', 'Pamela', '+2457812563', 0, 18, false, null, 0);
insert into driver (first_name, last_name, phone_number, experience, age, has_license, category, rating)
values ('Willis', 'Bruce', '+654789321', 15, 48, true, 'B', 5);
insert into driver (first_name, last_name, phone_number, experience, age, has_license, category, rating)
values ('Kidman', 'Nicole', '+456321456', 4, 24, true, 'A B', 5);
insert into driver (first_name, last_name, phone_number, experience, age, has_license, category, rating)
values ('Reeves', 'Keanu', '+369852147', 2, 28, true, 'B C', 3);


insert into car(model, color, number, owner_id)
values('Opel', 'red', 'a123qw111', 1);
insert into car(model, color, number, owner_id)
values('Lada', 'white', 'g222gg222', 2);
insert into car(model, color, number, owner_id)
values('BMW', 'Black', 'h444hh444', 2);
insert into car(model, color, number, owner_id)
values('Daewoo', 'Blue', 'w88ww888', 3);
insert into car(model, color, number, owner_id)
values('Mazda', 'Black', 's444ss444', 4);
insert into car(model, color, number, owner_id)
values('Volvo', 'Silver', 'a123aa123', 4);
insert into car(model, color, number, owner_id)
values('Bugatti', 'Grey', 'q456qq789', 6);
insert into car(model, color, number, owner_id)
values('Mazda', 'Grey', 's125df444', 5);


insert into ride(driver_id, car_id, date_ride, lasting)
values(1, 2, '2022-01-10', 2);
insert into ride(driver_id, car_id, date_ride, lasting)
values(1, 8, '2022-02-11', 1);
insert into ride(driver_id, car_id, date_ride, lasting)
values(2, 3, '2022-04-01', 20);
insert into ride(driver_id, car_id, date_ride, lasting)
values(2, 2, '2022-05-06', 10);
insert into ride(driver_id, car_id, date_ride, lasting)
values(2, 7, '2022-01-12', 8);
insert into ride(driver_id, car_id, date_ride, lasting)
values(3, 1, '2022-06-02', 3);
insert into ride(driver_id, car_id, date_ride, lasting)
values(4, 6, '2022-10-03', 4);
insert into ride(driver_id, car_id, date_ride, lasting)
values(4, 5, '2022-05-05', 10);
insert into ride(driver_id, car_id, date_ride, lasting)
values(5, 8, '2022-06-04', 12);
insert into ride(driver_id, car_id, date_ride, lasting)
values(6, 5, '2022-02-09', 5);
insert into ride(driver_id, car_id, date_ride, lasting)
values(6, 4, '2022-03-10', 7);
insert into ride(driver_id, car_id, date_ride, lasting)
values(6, 2, '2022-04-11', 14);
insert into ride(driver_id, car_id, date_ride, lasting)
values(2, 4, '2022-05-12', 7);