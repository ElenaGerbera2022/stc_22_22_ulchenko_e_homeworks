package ru.inno.ec.security.config.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.inno.ec.models.Group;
import ru.inno.ec.models.Post;
import ru.inno.ec.models.User;

import java.util.List;

public interface PostsRepository extends JpaRepository<Post, Long> {
    List<Post> findAllByGroup(Group group);
}
