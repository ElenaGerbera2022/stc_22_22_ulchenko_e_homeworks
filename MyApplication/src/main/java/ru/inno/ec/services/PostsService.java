package ru.inno.ec.services;
import ru.inno.ec.models.Post;

import java.util.List;

public interface PostsService {
    List<Post> getAllPosts();

    Post getPost(Long postId);

    void AddPost(Long groupId, Long userId, String message);
    List<Post> getPostByGroup(Long groupId);

    void deletePost(Long postId);

}
