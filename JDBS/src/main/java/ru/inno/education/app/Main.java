package ru.inno.education.app;

import com.zaxxer.hikari.HikariDataSource;
import ru.inno.education.models.Student;
import ru.inno.education.repositories.StudentsRepository;
import ru.inno.education.repositories.StudentsRepositoryJdbsImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
            HikariDataSource datasourse = new HikariDataSource();
            datasourse.setPassword(dbProperties.getProperty("db.password"));
            datasourse.setUsername(dbProperties.getProperty("db.username"));
            datasourse.setJdbcUrl(dbProperties.getProperty("db.url"));
            datasourse.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));
            StudentsRepository studentsRepository = new StudentsRepositoryJdbsImpl(datasourse);
//
//            List<Student> students = studentsRepository.findAll();
//            System.out.println(students);
//            for (Student student : students) {
//                System.out.println(student);
//            }

            Student student = Student.builder()
                    .email("666@mail.ru")
                    .password("666")
                    .firstName("Ffff")
                    .lastName("Fff")
                    .age(23)
                    .isWorker(true)
                    .average(4.1)
                    .build();
            System.out.println(student);
            studentsRepository.save(student);
            System.out.println(student);

    }
}