public class Main {
    public static void main(String[] args) {

        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("cars.txt");

        assert ("[а001аа111, а004аа111, а005аа111, а006аа111, а011аа111, а013аа111, а017аа111, а021аа111, а027аа111, а029аа111]".equals(carsRepository.filterCarToColorOrMileage("Black", 0)));
        assert ("[а002аа111, а029аа111, а030аа111]".equals(carsRepository.filterCarToColorOrMileage("Silver", 200)));

        assert (4 == carsRepository.filterDistinctCarPriceFromTo(100000, 300000));
        assert (3 == carsRepository.filterDistinctCarPriceFromTo(1000000, 3000000));

        assert ("[Blue, Red, White]".equals(carsRepository.filterColorOfMinPrice()));
        assert ("Optional[Blue]".equals(carsRepository.filterColorOfMinPriceOther()));

        assert (416750.0 == carsRepository.getAveragePrice("Camry"));
        assert (272400.0 == carsRepository.getAveragePrice("Lada"));

        assert ("[а002аа111, а029аа111, а030аа111]".equals(carsRepository.filterCarToColorOrMileageList("Silver", 200).toString()));
    }
}