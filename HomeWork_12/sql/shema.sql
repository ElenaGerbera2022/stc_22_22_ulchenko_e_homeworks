drop table  if exists ride;
drop table  if exists car;
drop table  if exists driver;

create table driver
(
    id    serial primary key,
    first_name   varchar(20),
    last_name    varchar(20),
    phone_number varchar(12),
    experience   int,
    age          integer not null,
    has_license      bool,
    category     varchar(6),
    rating       real
);

create table car
(
    id    serial primary key,
    model varchar(20),
    color varchar(20),
    number varchar(9) not null,
    owner_id integer not null,
    foreign key (owner_id) references driver(id)
);

create table ride
(
    driver_id integer not null,
    car_id integer not null,
    date_ride timestamp not null,
    lasting integer,
    foreign key (driver_id) references driver(id),
    foreign key (car_id) references car(id)
);