package ru.inno.ec.security.config.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Group;

public interface GroupRepository extends JpaRepository<Group, Long> {
}
