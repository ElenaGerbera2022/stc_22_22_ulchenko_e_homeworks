package ru.inno.education.injaction;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Properties;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Properties dbProperties = new Properties();

        dbProperties.load(new BufferedReader(
                new InputStreamReader(ru.inno.education.app.Main.class.getResourceAsStream("/db.properties"))));
        Connection connection = DriverManager.getConnection(
                dbProperties.getProperty("db.url"),
                dbProperties.getProperty("db.username"),
                dbProperties.getProperty("db.password"));
        Scanner scanner = new Scanner(System.in);

//        String lessonName = scanner.nextLine();
//        //String sql = "insert into student(email, password, first_name, last_name, age, isworker, average values()";
//        String sql = "insert into student(email values(?))";
//        PreparedStatement statement = connection.prepareStatement(sql);
//        statement.setString(lessonName);
//
//        statement.executeUpdate();
    }
}
