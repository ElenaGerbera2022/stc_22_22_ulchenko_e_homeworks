package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.ec.models.Group;
import ru.inno.ec.models.User;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.GroupsService;
import ru.inno.ec.services.PostsService;
import ru.inno.ec.services.UsersService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequiredArgsConstructor
@Controller
public class GroupsController {

    private final GroupsService groupsService;
    private final PostsService postsService;
    private final UsersService usersService;

    @GetMapping("/groups")
    public String getGroupsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) //разобраться с авторизацией!!!
    {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("groups", groupsService.getAllGroups());
        return "groups/groups_page";
    }

    @GetMapping("/groups/{group-id}")
    public String getGroupPage(@PathVariable("group-id") Long groupId,
                               @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        List<User> getNotInGroupUsers = groupsService.getNotInGroupUsers(groupId);
        model.addAttribute("group", groupsService.getGroup(groupId));
        model.addAttribute("notInGroupUsers", getNotInGroupUsers);
        model.addAttribute("inGroupUsers", groupsService.getInGroupUsers(groupId));
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("postsInGroup", postsService.getPostByGroup(groupId));
        return "groups/group_page";
    }

    @PostMapping("/groups")
    public String addGroup(Group group) {
        groupsService.addGroup(group);
        return "redirect:/groups";
    }

    @PostMapping("/groups/{group-id}/add")
    public String addUser(HttpServletRequest request,
                          @PathVariable("group-id") Long groupId) {
        Long userId = Long.parseLong(request.getParameter("user-id"));
        groupsService.addUserToGroup(groupId, userId);
        return "redirect:/groups/" + groupId;
    }

    @PostMapping("/groups/{group-id}/post")
    public String addUser(@PathVariable("group-id") Long groupId,
                          @AuthenticationPrincipal CustomUserDetails customUserDetails,
                          HttpServletRequest request) {
        String message = request.getParameter("message");
        postsService.AddPost(groupId, customUserDetails.getUser().getId(), message);
        return "redirect:/groups/" + groupId;
    }

    @GetMapping("/groups/{group-id}/delete")
    public String deleteGroup(@PathVariable("group-id") Long groupId) {
        groupsService.deleteGroup(groupId);
        return "redirect:/groups/";
    }

    @GetMapping("/groups/{group-id}/posts/{post-id}/delete/")
    public String deletePost(@PathVariable("group-id") Long groupId,
                             @PathVariable("post-id") Long postId
    ) {
        postsService.deletePost(postId);
        return "redirect:/groups/" + groupId;
    }

    @PostMapping("/groups/{group-id}/update")
    public String updateGroup(//HttpServletRequest request,
                              @PathVariable("group-id") Long groupId,
                              Group group) {
        groupsService.updateGroup(groupId, group);
        return "redirect:/groups/" + groupId;
    }

}