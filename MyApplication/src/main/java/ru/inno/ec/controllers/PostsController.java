package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.inno.ec.services.PostsService;

@RequiredArgsConstructor
@Controller
public class PostsController {
    private final PostsService postsService;

    @GetMapping("/posts")
    public String getPostsPage(Model model)
    {
        model.addAttribute("posts", postsService.getAllPosts());
        return "posts/posts_page";
    }

    @GetMapping("/posts/{post-id}")
    public String getPostPage(@PathVariable("post-id") Long id, Model model) {
        model.addAttribute("post", postsService.getPost(id));
        return "posts/post_page";
    }
}
