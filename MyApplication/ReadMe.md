Проект: 
    ФОРУМ

    Состав: 
    - Пользователи
    - Группы/Темы
    - Посты/Сообщения

Пользователи 2х ролей: ADMIN, USER
Права и возможности пользователей

ADMIN

    * Страница пользователей
        - просмотр всех пользователей
        - добавление пользователя
        - редактирование данных пользователя
        - удаление пользователя

    * Страница групп
        - просмотр всех групп
        - добавить группу
        - перейти в группу

    * Страница группы
        - изменить название/описание
        - добавить пользователя
        - добавить пост
        - удалить пост

    * Страница постов
        - Посмотреть все посты
        - перейти к группе с этим постом

USER

    * Страница пользователей
        - просмотр всех пользователей

    * Страница групп
        - просмотр всех групп
        - перейти в группу

    * Страница группы
        - добавить пост

    * Страница постов
        - Посмотреть все посты
        - перейти к группе с этим постом

