package ru.inno.ec.models;

import javax.persistence.*;

//import jakarta.persistence.Entity;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = {"group", "user"})
@ToString(exclude = {"group", "user"})
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String message;

    @Column(name = "post_time", updatable = false,
            columnDefinition = "timestamp(0)")
    @DateTimeFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    @CreationTimestamp
    private LocalDateTime postDT;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}

