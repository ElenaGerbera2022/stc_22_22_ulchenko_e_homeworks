package ru.inno.util.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.util.models.Car;
import ru.inno.util.repository.CarsRepository;
import ru.inno.util.repository.CarsRepositoryJdbsImpl;

@Parameters(separators = "=")
public class Program {

    @Parameter(names = {"-action"})
    private String action;

    public static void main(String[] args) {
        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(
                            Program.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        HikariDataSource setDataSource = new HikariDataSource();
        setDataSource.setPassword(dbProperties.getProperty("db.password"));
        setDataSource.setUsername(dbProperties.getProperty("db.username"));
        setDataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        setDataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        CarsRepository carsRepository = new CarsRepositoryJdbsImpl(setDataSource);
        Program program = new Program();

        JCommander.newBuilder()
                .addObject(program)
                .build()
                .parse(args);
        System.out.println(program.action);

        if (program.action.equalsIgnoreCase("read")) {
            List<Car> cars = carsRepository.findAll();
            for (Car car : cars) {
                System.out.println(car);
            }
        } else if (program.action.equalsIgnoreCase("write")) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Input car..");
            System.out.println("..model");
            String carModel = scanner.nextLine();
            System.out.println("..Color");
            String carColor = scanner.nextLine();
            System.out.println("..Number");
            String carNumber = scanner.nextLine();
            System.out.println("..DriverID");
            Integer carDriverId = scanner.nextInt();

            Car car2 = Car.builder()
                    .model(carModel)
                    .color(carColor)
                    .number(carNumber)
                    .driver_id(carDriverId)
                    .build();
            carsRepository.save(car2);
        } else {
            System.out.println("Command not found");
        };
    }
}
