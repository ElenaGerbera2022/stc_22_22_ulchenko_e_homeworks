import java.io.*;
import java.util.Comparator;
import java.util.List;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private final String fileName;
    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }
    @Override
    public List<Car> readAllCars() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(this::parseCar)
                    .toList();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

//    public static String readFirstLineFromFile(String path) throws IOException {
//        try (BufferedReader br =
//                     new BufferedReader(new FileReader(path))) {
//            return br.readLine();
//        }
//    }

    @Override
    public Car parseCar(String line) {
        String[] parts = line.split("\\|");
        String num = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);
        return new Car(num, model, color, mileage, price);
    }

    @Override
    public Long filterDistinctCarPriceFromTo(int from, int to) {
        List<Car> allCars = readAllCars();
        return
                allCars.stream()
                        .filter(car -> ((car.getPrice() > from) && (car.getPrice() < to)))
                        .map(Car::getModel)
                        .distinct()
                        .count();
    }

    @Override
    public Double getAveragePrice(String modelCar) {
        List<Car> allCars = readAllCars();
        return allCars
                .stream()
                .filter(car -> (car.getModel().equals(modelCar)))
                .mapToInt(Car::getPrice)
                .average()
                .getAsDouble();
    }

    @Override
    public String filterCarToColorOrMileage(String color, Integer mileage) {
        List<Car> allCars = readAllCars();
        return
                allCars.stream()
                        .filter(car -> (car.getColor().equals(color) || car.getMileage().equals(mileage)))
                        .map(Car::getNum)
                        .toList()
                        .toString();
    }


    public List<String> filterCarToColorOrMileageList(String color, Integer mileage) {
        List<Car> allCars = readAllCars();
        return
                allCars.stream()
                        .filter(car -> (car.getColor().equals(color) || car.getMileage().equals(mileage)))
                        .map(Car::getNum)
                        .toList();
                        //.toString();
    }

    @Override
    public String filterColorOfMinPrice() {
        List<Car> allCars = readAllCars();
        return allCars.stream()//3
                .filter(car -> car.getPrice().equals (
                        allCars.stream()
                                .mapToInt(Car::getPrice)
                                .min()
                                .getAsInt()))
                .map(Car::getColor)
                .toList()
                .toString();
    }

    public String filterColorOfMinPriceOther() {
    List<Car> allCars = readAllCars();
        return allCars.stream()//3
                .min(Comparator.comparing(Car::getPrice))
            .map(Car::getColor)
                .toString();
    }
}
