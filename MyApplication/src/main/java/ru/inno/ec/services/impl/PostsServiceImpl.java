package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.models.Group;
import ru.inno.ec.models.Post;
import ru.inno.ec.security.config.repositories.GroupRepository;
import ru.inno.ec.security.config.repositories.PostsRepository;
import ru.inno.ec.security.config.repositories.UsersRepository;
import ru.inno.ec.services.PostsService;

import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
@Service
public class PostsServiceImpl implements PostsService {

    private final GroupRepository groupsRepository;
    private final UsersRepository usersRepository;
    private final PostsRepository postsRepository;

    @Override
    public List<Post> getAllPosts() {
        return postsRepository.findAll();
    }

    @Override
    public Post getPost(Long postId) {
        return postsRepository.findById(postId).orElseThrow();
    }

    @Override
    public void AddPost(Long groupId, Long userId, String message) {
        Post newPost = Post.builder()
                .postDT(LocalDateTime.now())
                .message(message)
                .group(groupsRepository.findById(groupId).orElseThrow()) //groupId(groupId)
                .user(usersRepository.findById(userId).orElseThrow())
                .build();
        postsRepository.save(newPost);
    }

    @Override
    public List<Post> getPostByGroup(Long groupId) {
        Group group = groupsRepository.findById(groupId).orElseThrow();
        return postsRepository.findAllByGroup(group);
    }

    @Override
    public void deletePost(Long postId) {

        postsRepository.deleteById(postId);
    }
}
