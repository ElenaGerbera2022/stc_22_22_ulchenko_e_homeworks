package ru.inno.education.repositories;

import ru.inno.education.models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;


public class StudentsRepositoryJdbsImpl implements StudentsRepository {
    //Language=SQL
    private static final String SQL_SELECT_ALL = "select * from student order by id";

    //Language=SQL
    private static final String SQL_INSERT = "insert into student(email, password, first_name, last_name, age, isworker, average) " +
            "values(?, ?, ?, ?, ?, ?, ?)";
    //String sql = "insert into student(email, password, first_name, last_name, age, isworker, average values()";

    private DataSource dataSource;

    public StudentsRepositoryJdbsImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Student> userRowMapper = row -> {
        try {
            return Student.builder()
                    .id(row.getLong("id"))
                    .email(row.getString("email"))
                    .password(row.getString("password"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .age(row.getInt("age"))
                    .isWorker(row.getBoolean("isworker"))
                    .average(row.getDouble("average"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public List<Student> findAll() {
        List<Student> students = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Student student = userRowMapper.apply(resultSet);
                    students.add(student);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return students;
    }

    @Override
    public void save(Student student) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, student.getEmail());
            preparedStatement.setString(2, student.getPassword());
            preparedStatement.setString(3, student.getFirstName());
            preparedStatement.setString(4, student.getLastName());
            preparedStatement.setInt(5, student.getAge());
            preparedStatement.setBoolean(6, student.getIsWorker());
            preparedStatement.setDouble(7, student.getAverage());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert student");
            }

            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    student.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't obtain generated id");
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
